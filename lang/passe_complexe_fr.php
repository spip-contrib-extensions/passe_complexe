<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/passe_complexe.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_common' => 'Liste des mots communs interdis dans les mots de passe. <small>(Séparés par des virgules)</small> :',
	'cfg_descriptif' => 'Le plugin testeur de mot de passe permet de tester la &quot;force&quot; d’un mot de passe.<br/>Les tests sont fait sur différents critès comme la longueur du mot de passe, s’ils contiennent des caractères spéciaux, etc... <br/>Vous pouvez aussi spécifier une liste de mots interdits dans un mot de passe.',
	'cfg_false' => 'Désactiver',
	'cfg_longueur' => 'Longueur minimum du mot de passe : ',
	'cfg_passe_complexe' => 'Configurer le plugin Mot de Passe Compliquer',
	'cfg_showpercent' => 'Afficher le pourcentage de complexité du mot de passe',
	'cfg_titre_parametrages' => 'Testeur de Mot de Passe',
	'cfg_true' => 'Activer',
	'common' => 'motdepasse,azerty,qwertzui',
	'court' => 'Trop Court',

	// F
	'faible' => 'Faible',
	'fort' => 'Fort',

	// M
	'mot_interdit' => 'Le mot est interdit',
	'motpassecomplexe_actif' => 'Vous avez le plugin "Mot de passe complexe" actif, il faut changer la longeur du mot de passe dans ce plugin',
	'moyen' => 'Moyen',

	// N
	'nb_mini' => 'Nombre de caractères minimum : '
);
